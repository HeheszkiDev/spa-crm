var webpack = require('webpack');

module.exports = {
    providePlugin: new webpack.ProvidePlugin({
        $: 'jquery',
        'window.$': 'jquery',
        'window.jQuery': 'jquery'
    })
};