var ExtractTextPlugin = require('extract-text-webpack-plugin');
var packageJSON = require('./package.json');
var path = require('path');
var webpack = require('webpack');
var plugins = require('./webpack.plugins.js');
var boostrapEntryPoint = require('./webpack.bootstrap.config');
const PATHS = {
    build: path.join(__dirname, 'target', 'classes', 'META-INF', 'resources', 'webjars', packageJSON.name, packageJSON.version)
};

var bootstrapConfig = boostrapEntryPoint.dev;
module.exports = {
    // entry: './app/index.js',
    entry: {
        app: './app/index.js',
        bootstrap: bootstrapConfig
    },
    output: {
        path: PATHS.build,
        filename: '[name]-bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.scss$/, use: ExtractTextPlugin.extract({
                fallbackLoader: 'style-loader',
                loader: ['css-loader', 'sass-loader'],
                publicPath: PATHS.build
            }),
            },
            {test: /\.(woff2?|svg)$/, use: 'url-loader?limit=10000'},
            {test: /\.(ttf|eot)$/, use: 'file-loader'},
            {test: /bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/, use: 'imports-loader?jQuery=jquery'}
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: "style.css",
            disable: false,
            allChunks: true
        })
    ]

};