require('./libs.js');

angular
    .module('crm', ['ui.bootstrap', 'ui.router', 'ngStorage', 'datetime'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");
        // app routes
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'template/main.html',
                controller: 'MainController',
                controllerAs: 'vm'
            })
            .state('customers', {
                url: '/customers',
                templateUrl: 'template/customers.html',
                controller: 'CustomerController',
                controllerAs: 'vm'
            })
            .state('tags', {
                url: '/tags',
                templateUrl: 'template/tags.html',
                controller: 'TagController',
                controllerAs: 'vm'
            })
            .state('tagsCustomers', {
                url: '/tags/{tagName}',
                templateUrl: 'template/customers.html',
                controller: 'TagCustomerController',
                controllerAs: 'vm'
            })
            .state('customerDetails', {
                url: '/customers/{id}',
                templateUrl: 'template/customerDetails.html',
                controller: 'CustomerDetailsController',
                controllerAs: 'vm'
            })
            .state('customerEvents', {
                url: '/customers/{id}/events',
                templateUrl: 'template/events.html',
                controller: 'EventController',
                controllerAs: 'vm'
            })
            .state('eventsList', {
                url: '/events',
                templateUrl: 'template/eventsList.html',
                controller: 'EventsListController',
                controllerAs: 'vm'
            })
            .state('customerNotes', {
                url: '/customers/{id}/notes',
                templateUrl: 'template/notes.html',
                controller: 'NoteController',
                controllerAs: 'vm'
            })
            .state('customerFinancial', {
                url: '/customers/{id}/financials',
                templateUrl: 'template/financials.html',
                controller: 'FinancialController',
                controllerAs: 'vm'
            })
            .state('financialList', {
                url: '/reports',
                templateUrl: 'template/financialList.html',
                controller: 'FinancialListController',
                controllerAs: 'vm'
            })
            .state('eventTypes', {
                url: '/types',
                templateUrl: 'template/eventTypes.html',
                controller: 'EventTypeController',
                controllerAs: 'vm'
            })
            .state('users', {
                url: '/users',
                templateUrl: 'template/users.html',
                controller: 'UsersController',
                controllerAs: 'vm'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'template/login.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            })
            .state('register', {
                url: '/register',
                templateUrl: 'template/register.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            })
            .state('editUser', {
                url: '/settings',
                templateUrl: 'template/editUser.html',
                controller: 'SettingsController',
                controllerAs: 'vm'
            })
            .state('password', {
                url: '/settings/password',
                templateUrl: 'template/password.html',
                controller: 'PasswordController',
                controllerAs: 'vm'
            });
    })
    .controller('MainController', require('./controllers/MainController.js'))
    .controller('CustomerController', require('./controllers/CustomerController.js'))
    .controller('EventController', require('./controllers/EventController.js'))
    .controller('NoteController', require('./controllers/NoteController.js'))
    .controller('FinancialController', require('./controllers/FinancialController.js'))
    .controller('FinancialListController', require('./controllers/FinancialListController.js'))
    .controller('LoginController', require('./controllers/LoginController.js'))
    .controller('CustomerModalDialogController', require('./controllers/CustomerModalDialogController.js'))
    .controller('EventModalDialogController', require('./controllers/EventModalDialogController.js'))
    .controller('NoteModalDialogController', require('./controllers/NoteModalDialogController.js'))
    .controller('FinancialModalDialogController', require('./controllers/FinancialModalDialogController.js'))
    .controller('CustomerDetailsController', require('./controllers/CustomerDetailsController.js'))
    .controller('TagController', require('./controllers/TagController.js'))
    .controller('TagCustomerController', require('./controllers/TagCustomerController.js'))
    .controller('MenuController', require('./controllers/MenuController.js'))
    .controller('EventTypeController', require('./controllers/EventTypeController.js'))
    .controller('UsersController', require('./controllers/UsersController.js'))
    .controller('EventsListController', require('./controllers/EventsListController.js'))
    .controller('SettingsController', require('./controllers/SettingsController.js'))
    .controller('PasswordController', require('./controllers/PasswordController.js'))
    .factory('CustomerFactory', require('./factories/CustomerFactory.js'))
    .factory('EventFactory', require('./factories/EventFactory.js'))
    .factory('NoteFactory', require('./factories/NoteFactory.js'))
    .factory('FinancialFactory', require('./factories/FinancialFactory.js'))
    .factory('TagFactory', require('./factories/TagFactory.js'))
    .factory('EventTypeFactory', require('./factories/EventTypeFactory.js'))
    .factory('DashboardFactory', require('./factories/DashboardFactory.js'))
    .factory('UsersFactory', require('./factories/UsersFactory.js'))
    .factory('AuthenticationService', require('./services/AuthenticationService.js'))
    .service('CustomerService', require('./services/CustomerService.js'))
    .service('EventService', require('./services/EventService.js'))
    .service('NoteService', require('./services/NoteService.js'))
    .service('FinancialService', require('./services/FinancialService.js'))
    .run(run);

function run($rootScope, $http, $location, $localStorage) {
    // keep user logged in after page refresh
    if ($localStorage.currentUser) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
    }

    // redirect to login page if not logged in and trying to access a restricted page
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        $rootScope.showNavBar = false;
        var publicPages = ['/login', '/register'];
        var restrictedPage = publicPages.indexOf($location.path()) === -1;
        if (restrictedPage && !$localStorage.currentUser) {
            $location.path('/login');
        }
    });
}