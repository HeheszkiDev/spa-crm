module.exports = function () {

    var event;

    var passEvent = function(obj){
        console.log(obj);
        event = obj;
    };

    var retrieveEvent = function () {
        console.log(event);
        return event;
    };

    return {
        passEvent: passEvent,
        retrieveEvent: retrieveEvent
    };
};