module.exports = function () {

    var customer;
    var customerId = 0;

    var passCustomer = function(obj){
        customer = obj;
    };

    var retrieveCustomer = function () {
        return customer;
    };

    var passCustomerId = function(obj){
        console.log(obj);
        customerId = obj;
    };

    var retrieveCustomerId = function () {
        return customerId;
    };

    return {
        passCustomer: passCustomer,
        retrieveCustomer: retrieveCustomer,
        passCustomerId: passCustomerId,
        retrieveCustomerId: retrieveCustomerId
    };
};