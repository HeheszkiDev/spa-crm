module.exports = function () {

    var financial;

    var passFinancial = function(obj){
        console.log(obj);
        financial = obj;
    };

    var retrieveFinancial = function () {
        console.log(financial);
        return financial;
    };

    return {
        passFinancial: passFinancial,
        retrieveFinancial: retrieveFinancial
    };
};