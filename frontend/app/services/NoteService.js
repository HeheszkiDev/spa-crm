module.exports = function () {

    var note;

    var passNote = function(obj){
        console.log(obj);
        note = obj;
    };

    var retrieveNote = function () {
        console.log(note);
        return note;
    };

    return {
        passNote: passNote,
        retrieveNote: retrieveNote
    };
};