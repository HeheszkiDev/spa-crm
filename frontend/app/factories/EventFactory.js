module.exports = function ($http) {
    'ngInject';
    var factory = {};
    factory.getEvents = function () {
        return $http.get(
            'http://localhost:8090/api/event/'
        )
    };
    factory.getEventsByCustomerId = function (customerId) {
        return $http.get(
            'http://localhost:8090/api/event/customer/' + customerId
        )
    };
    factory.createEvent = function (event) {
        return $http.post('http://localhost:8090/api/event/', event)
    };
    factory.getEvent = function (id) {
        return $http.get('http://localhost:8090/api/event/' + id)
    };
    factory.deleteEvent = function (id) {
        return $http.delete('http://localhost:8090/api/event/' + id)
    };
    factory.updateEvent = function (event) {
        return $http.put('http://localhost:8090/api/event/' + event.id, event)
    };


    return factory;

};