module.exports = function ($http) {
    'ngInject';
    var factory = {};
    factory.getFinancials = function () {
        return $http.get(
            'http://localhost:8090/api/financial/'
        )
    };
    factory.getFinancialsByCustomerId = function (customerId) {
        return $http.get(
            'http://localhost:8090/api/financial/customer/' + customerId
        )
    };
    factory.createFinancial = function (financial) {
        return $http.post('http://localhost:8090/api/financial/', financial)
    };
    factory.getFinancial = function (id) {
        return $http.get('http://localhost:8090/api/financial/' + id)
    };
    factory.deleteFinancial = function (id) {
        return $http.delete('http://localhost:8090/api/financial/' + id)
    };
    factory.updateFinancial = function (financial) {
        return $http.put('http://localhost:8090/api/financial/' + financial.id, financial)
    };


    return factory;

};