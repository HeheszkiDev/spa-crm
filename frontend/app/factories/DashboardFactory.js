module.exports = function ($http) {
    'ngInject';
    var factory = {};
    factory.getDashboardStats = function () {
        return $http.get(
            'http://localhost:8090/api/stats/'
        )
    };

    return factory;
};
