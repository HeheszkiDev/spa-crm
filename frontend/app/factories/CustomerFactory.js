module.exports = function ($http, $localStorage) {
    'ngInject';
    var factory = {};
    factory.getCustomers = function () {
        return $http.get(
            'http://localhost:8090/api/customer/'
        )
    };
    factory.createCustomer = function (customer) {
        return $http.post('http://localhost:8090/api/customer/', customer)
    };
    factory.getCustomer = function (id) {
        return $http.get('http://localhost:8090/api/customer/' + id)
    };
    factory.deleteCustomer = function (id) {
        return $http.delete('http://localhost:8090/api/customer/' + id)
    };
    factory.updateCustomer = function (customer) {
        return $http.put('http://localhost:8090/api/customer/' + customer.id, customer)
    };

    return factory;
};
