module.exports = function ($http) {
    'ngInject';
    var factory = {};
    factory.getNotes = function () {
        return $http.get(
            'http://localhost:8090/api/note/'
        )
    };
    factory.getNotesByCustomerId = function (customerId) {
        return $http.get(
            'http://localhost:8090/api/note/customer/' + customerId
        )
    };
    factory.createNote = function (note) {
        return $http.post('http://localhost:8090/api/note/', note)
    };
    factory.getNote = function (id) {
        return $http.get('http://localhost:8090/api/note/' + id)
    };
    factory.deleteNote = function (id) {
        return $http.delete('http://localhost:8090/api/note/' + id)
    };
    factory.updateNote = function (note) {
        return $http.put('http://localhost:8090/api/note/' + note.id, note)
    };


    return factory;

};