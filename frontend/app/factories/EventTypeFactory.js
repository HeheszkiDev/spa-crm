module.exports = function ($http) {
    'ngInject';
    var factory = {};
    factory.getEventTypes = function () {
        return $http.get(
            'http://localhost:8090/api/eventType/'
        )
    };
    factory.createEventType = function (eventType) {
        return $http.post('http://localhost:8090/api/eventType/', {eventName: eventType})
    };
    factory.deleteEventType = function (id) {
        return $http.delete('http://localhost:8090/api/eventType/' + id)
    };

    return factory;
};
