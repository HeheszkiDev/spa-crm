module.exports = function ($http) {
    'ngInject';
    var factory = {};
    factory.getAllTags = function () {
        return $http.get(
            'http://localhost:8090/api/tag/'
        )
    };

    factory.getAllCustomerTags = function (customerId) {
        return $http.get(
            'http://localhost:8090/api/tag/customer/' + customerId
        )
    };

    factory.getAllCustomerFromTag = function (tagName) {
        return $http.get(
            'http://localhost:8090/api/tag/' + tagName
        )
    };


    factory.addCustomerToTag = function (customerId, tagName) {
        return $http.post('http://localhost:8090/api/tag/customer', {customerId: customerId, tagName: tagName})
    };

    factory.removeCustomerFromTag = function (customerId, tagName) {
        return $http.post('http://localhost:8090/api/tag/customer/remove', {customerId: customerId, tagName: tagName})
    };

    factory.deleteTag = function (id) {
        return $http.delete('http://localhost:8090/api/tag/' + id)
    };

    return factory;
};
