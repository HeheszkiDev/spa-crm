module.exports = function ($http) {
    'ngInject';
    var factory = {};
    factory.getUsersList = function () {
        return $http.get(
            'http://localhost:8090/api/users/'
        )
    };
    factory.getLoggedUser = function () {
        return $http.get(
            'http://localhost:8090/user'
        )
    };
    factory.updateUser = function (user) {
        return $http.post('http://localhost:8090/api/users/settings', user)
    };

    factory.updatePassword = function (data) {
        return $http.post('http://localhost:8090/api/users/settings/password', data)
    };

    return factory;
};
