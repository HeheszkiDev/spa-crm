module.exports = function ($scope, $location, $uibModal, CustomerFactory, CustomerService) {
    'ngInject';


    $scope.standardView = true;




    var retrieveCustomers = function () {
        CustomerFactory.getCustomers().then(function (response) {
            $scope.customersList = response.data;
        });
    };

    retrieveCustomers();

    $scope.showModal = function () {
        $uibModal.open({
            templateUrl: 'customerDetailsModal.html',
            controller: 'CustomerModalDialogController'
        })
        .result.then(
            function () {
                retrieveCustomers();
        },
        function () {
            console.log('Error during modal processing');
        });
    };

    $scope.showUpdateModal = function (customer) {
        $uibModal.open({
            templateUrl: 'updateCustomerDetailsModal.html',
            controller: 'CustomerModalDialogController',
            resolve: {
                customer: function () {
                    CustomerService.passCustomer(customer);
                }
            }
        })
            .result.then(
            function () {
                retrieveCustomers();
            },
            function () {
                console.log('Error during modal processing');
            });
    };

    $scope.deleteCustomer = function(customerId){
        CustomerFactory.deleteCustomer(customerId).then(function (response) {
            retrieveCustomers();
        });
    }

};
