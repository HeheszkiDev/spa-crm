module.exports = function ($scope, $stateParams, $location, $uibModal, CustomerFactory, CustomerService, TagFactory) {
    'ngInject';

    $scope.standardView = false;

    var tagName = $stateParams.tagName;

    var retrieveCustomers = function (tagName) {
        TagFactory.getAllCustomerFromTag(tagName).then(function (response) {
            $scope.customersList = response.data;
        });
    };

    retrieveCustomers(tagName);

    $scope.showModal = function () {
        $uibModal.open({
            templateUrl: 'customerDetailsModal.html',
            controller: 'CustomerModalDialogController'
        })
            .result.then(
            function () {
                retrieveCustomers(tagName);
            },
            function () {
                console.log('Error during modal processing');
            });
    };

    $scope.showUpdateModal = function (customer) {
        $uibModal.open({
            templateUrl: 'updateCustomerDetailsModal.html',
            controller: 'CustomerModalDialogController',
            resolve: {
                customer: function () {
                    CustomerService.passCustomer(customer);
                }
            }
        })
            .result.then(
            function () {
                retrieveCustomers(tagName);
            },
            function () {
                console.log('Error during modal processing');
            });
    };

    $scope.deleteCustomer = function(customerId){
        CustomerFactory.deleteCustomer(customerId).then(function (response) {
            retrieveCustomers(tagName);
        });
    }

};
