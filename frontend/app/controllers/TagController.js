module.exports = function ($scope, TagFactory, CustomerFactory, CustomerService) {
    'ngInject';

    var getAllTags = function () {
        TagFactory.getAllTags().then(function (response) {
            $scope.tags = response.data;
        });
    };

    $scope.deleteTag = function (tagId) {
        TagFactory.deleteTag(tagId).then(function (response) {
            getAllTags();
        });
    };

    getAllTags();
};
