module.exports = function ($scope, $location, UsersFactory) {
    'ngInject';
    $scope.error = false;

    var loggedUser = function () {
        UsersFactory.getLoggedUser().then(function (response) {
            $scope.user = response.data;
        });
    };

    $scope.updateSettingsForm = function () {
        var data = $scope.user;
        UsersFactory.updateUser(data).then(function (response) {
                loggedUser();
                $scope.error = false;
            },
            function () {
                $scope.error = true;
            });
    };

    loggedUser();
};
