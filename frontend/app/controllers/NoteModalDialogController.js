module.exports = function ($scope, $uibModalInstance, $location, NoteFactory, NoteService, CustomerService) {

    var retrieveNoteFromService = function () {
        $scope.note = NoteService.retrieveNote();
        $scope.customerId = CustomerService.retrieveCustomerId();
    };


    retrieveNoteFromService();

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.submitNoteForm = function () {
        /* while compiling form , angular created this object*/
        $scope.fields.customerId = $scope.customerId;
        var data = $scope.fields;
        retrieveNoteFromService();
        /* post to server*/
        NoteFactory.createNote(data).then(function (response) {
            $uibModalInstance.close();
        });
    };

    $scope.updateNoteForm = function () {
        /* while compiling form , angular created this object*/
        var data = $scope.note;
        data.customerId = $scope.customerId;

        // update entity
        NoteFactory.updateNote(data).then(function (response) {
            $uibModalInstance.close();
        });
    };
};