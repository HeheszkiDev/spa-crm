module.exports = function ($scope, $uibModalInstance, $location, CustomerFactory, CustomerService) {

    $scope.customer = CustomerService.retrieveCustomer();

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.submitCustomerForm = function () {
        console.log("submit");
        /* while compiling form , angular created this object*/
        var data = $scope.fields;

        /* post to server*/
        CustomerFactory.createCustomer(data).then(function (response) {
            $uibModalInstance.close();
        });
    };

    $scope.updateCustomerForm = function () {
        /* while compiling form , angular created this object*/
        var data = $scope.customer;
        // update entity
        CustomerFactory.updateCustomer(data).then(function (response) {
            $uibModalInstance.close();
        });
    };
};