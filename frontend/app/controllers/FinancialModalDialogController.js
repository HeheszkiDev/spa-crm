module.exports = function ($scope, $uibModalInstance, $location, FinancialFactory, FinancialService, CustomerService) {

    var retrieveFinancialFromService = function () {
        $scope.financial = FinancialService.retrieveFinancial();
        $scope.customerId = CustomerService.retrieveCustomerId();
    };


    retrieveFinancialFromService();

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.submitFinancialForm = function () {
        /* while compiling form , angular created this object*/
        $scope.fields.customerId = $scope.customerId;
        var data = $scope.fields;
        retrieveFinancialFromService();
        /* post to server*/
        FinancialFactory.createFinancial(data).then(function (response) {
            $uibModalInstance.close();
        });
    };

    $scope.updateFinancialForm = function () {
        /* while compiling form , angular created this object*/
        var data = $scope.financial;
        data.customerId = $scope.customerId;

        // update entity
        FinancialFactory.updateFinancial(data).then(function (response) {
            $uibModalInstance.close();
        });
    };
};