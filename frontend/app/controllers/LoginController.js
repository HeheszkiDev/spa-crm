module.exports = function ($location, AuthenticationService, $scope) {
    var vm = this;

    vm.login = login;

    initController();

    function initController() {
        // reset login status
        AuthenticationService.Logout();
    };

    function login() {
        vm.loading = true;
        AuthenticationService.Login(vm.username, vm.password, function (result) {
            if (result === true) {
                $location.path('/');
            } else {
                vm.error = 'Username or password is incorrect';
                vm.loading = false;
            }
            vm.showError = !result;
        });
    };


    $scope.submitRegisterForm = function () {
        /* while compiling form , angular created this object*/
        var data = $scope.fields;

        /* post to server*/
        AuthenticationService.Register(data).then(function (response) {
            $location.path('/');
        });
    };
};
