module.exports = function ($scope, $location, UsersFactory, AuthenticationService) {
    'ngInject';
    $scope.error = false;

    var loggedUser = function () {
        UsersFactory.getLoggedUser().then(function (response) {
            $scope.user = response.data;
        });
    };

    $scope.updatePasswordForm = function () {
        var data = $scope.fields;
        data.username = $scope.user.username;
        UsersFactory.updatePassword(data).then(function (response) {
                $scope.error = false;
                AuthenticationService.Logout();
                $location.path("/");
            },
            function () {
                $scope.error = true;
            });
    };

    loggedUser();
};
