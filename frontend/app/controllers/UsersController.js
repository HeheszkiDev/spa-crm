module.exports = function ($scope, UsersFactory) {

    var retrieveUsers = function () {
        UsersFactory.getUsersList().then(function (response) {
            $scope.users = response.data;
        });
    };

    retrieveUsers();

};