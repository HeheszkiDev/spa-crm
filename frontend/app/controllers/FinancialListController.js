module.exports = function ($scope, FinancialFactory) {


    var retrieveReports = function () {
        FinancialFactory.getFinancials().then(function (response) {
            $scope.financialList = response.data;
        });
    };

    retrieveReports();
};