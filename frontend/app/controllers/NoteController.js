module.exports = function ($scope, $location, $stateParams, $uibModal, NoteFactory, CustomerService, NoteService) {

    var customerId = $stateParams.id;

    CustomerService.passCustomerId(customerId);

    var retrieveNotes = function () {
        NoteFactory.getNotes().then(function (response) {
            $scope.noteList = response.data;
        });
    };

    var retrieveNotesByCustomerId = function (customerId) {
        NoteFactory.getNotesByCustomerId(customerId).then(function (response) {
            $scope.noteList = response.data;
        });
    };

    retrieveNotesByCustomerId(customerId);

    $scope.showModal = function () {
        $uibModal.open({
            templateUrl: 'createNoteModal.html',
            controller: 'NoteModalDialogController'
        })
            .result.then(
            function () {
                retrieveNotesByCustomerId(customerId);
            },
            function () {
                console.log('Error during modal processing');
            });
    };

    $scope.showUpdateModal = function (note) {
        $uibModal.open({
            templateUrl: 'updateNoteModal.html',
            controller: 'NoteModalDialogController',
            resolve: {
                note: function () {
                    NoteService.passNote(note);
                }
            }
        })
            .result.then(
            function () {
                retrieveNotesByCustomerId(customerId);
                NoteService.passNote(note);
            },
            function () {
                console.log('Error during modal processing');
            });
    };

    $scope.deleteNote = function(id){
        NoteFactory.deleteNote(id).then(function (response) {
            retrieveNotesByCustomerId(customerId);
        });
    }


};