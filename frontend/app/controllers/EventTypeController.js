module.exports = function ($scope, EventTypeFactory) {

    var retrieveEventTypes = function () {
        EventTypeFactory.getEventTypes().then(function (response) {
            $scope.eventTypes = response.data;
        });
    };

    $scope.deleteEventType = function (id) {
        EventTypeFactory.deleteEventType(id).then(function (response) {
            retrieveEventTypes();
        });
    };

    $scope.saveEventType = function () {
        var newEventType = $scope.newEventType;
        if (newEventType.length > 0){
            EventTypeFactory.createEventType(newEventType).then(function (response) {
                retrieveEventTypes();
                $scope.newEventType = '';
            });
        }
    };

    retrieveEventTypes();
};