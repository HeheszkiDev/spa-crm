module.exports = function ($scope, $location, $stateParams, $uibModal, EventTypeFactory, EventFactory, CustomerService, EventService) {

    var customerId = $stateParams.id;

    CustomerService.passCustomerId(customerId);

    var retrieveEvents = function () {
        EventFactory.getEvents().then(function (response) {
            $scope.eventList = response.data;
        });
    };

    var retrieveEventsByCustomerId = function (customerId) {
        EventFactory.getEventsByCustomerId(customerId).then(function (response) {
            $scope.eventList = response.data;
        });
    };

    retrieveEventsByCustomerId(customerId);

    $scope.showModal = function () {
        $uibModal.open({
            templateUrl: 'createEventModal.html',
            controller: 'EventModalDialogController'
        })
            .result.then(
            function () {
                retrieveEventsByCustomerId(customerId);
            },
            function () {
                console.log('Error during modal processing');
            });
    };

    $scope.showUpdateModal = function (event) {
        $uibModal.open({
            templateUrl: 'updateEventModal.html',
            controller: 'EventModalDialogController',
            resolve: {
                event: function () {
                    EventService.passEvent(event);
                }
            }
        })
            .result.then(
            function () {
                retrieveEventsByCustomerId(customerId);
                EventService.passEvent(event);
            },
            function () {
                console.log('Error during modal processing');
            });
    };

    $scope.deleteEvent = function(id){
        EventFactory.deleteEvent(id).then(function (response) {
            retrieveEventsByCustomerId(customerId);
        });
    }


};