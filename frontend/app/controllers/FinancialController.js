module.exports = function ($scope, $location, $stateParams, $uibModal, FinancialFactory, CustomerService, FinancialService) {

    var customerId = $stateParams.id;

    CustomerService.passCustomerId(customerId);

    var retrieveFinancials = function () {
        FinancialFactory.getFinancials().then(function (response) {
            $scope.financialList = response.data;
        });
    };

    var retrieveFinancialsByCustomerId = function (customerId) {
        FinancialFactory.getFinancialsByCustomerId(customerId).then(function (response) {
            $scope.financialList = response.data;
        });
    };

    retrieveFinancialsByCustomerId(customerId);

    $scope.showModal = function () {
        $uibModal.open({
            templateUrl: 'createFinancialModal.html',
            controller: 'FinancialModalDialogController'
        })
            .result.then(
            function () {
                retrieveFinancialsByCustomerId(customerId);
            },
            function () {
                console.log('Error during modal processing');
            });
    };

    $scope.showUpdateModal = function (financial) {
        $uibModal.open({
            templateUrl: 'updateFinancialModal.html',
            controller: 'FinancialModalDialogController',
            resolve: {
                financial: function () {
                    FinancialService.passFinancial(financial);
                }
            }
        })
            .result.then(
            function () {
                retrieveFinancialsByCustomerId(customerId);
                FinancialService.passFinancial(financial);
            },
            function () {
                console.log('Error during modal processing');
            });
    };

    $scope.deleteFinancial = function(id){
        FinancialFactory.deleteFinancial(id).then(function (response) {
            retrieveFinancialsByCustomerId(customerId);
        });
    }


};