module.exports = function ($scope, $localStorage) {

    if ($localStorage.currentUser!=null){
        var groupPart = $localStorage.currentUser.token.split('.')[1];
        var obj = JSON.parse(atob(groupPart));
        var isAdmin = false;
        for (var i=0; i<obj.roles.length; i++){
            if (obj.roles[i]==='ROLE_ADMIN') {
                isAdmin = true;
            }
        }
        $scope.isAdmin = isAdmin;
    }
    else {
        $scope.isAdmin = false;
    }

};