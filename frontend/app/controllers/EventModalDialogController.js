module.exports = function ($scope, $uibModalInstance, $location, EventFactory, EventTypeFactory, EventService, CustomerService) {

    var retrieveEventFromService = function () {
        $scope.event = EventService.retrieveEvent();
        $scope.customerId = CustomerService.retrieveCustomerId();
    };

    $scope.init = function () {
        // initialize event types on controller startup
        retrieveEventTypes();
    };

    var retrieveEventTypes = function () {
        EventTypeFactory.getEventTypes().then(function (response) {
            $scope.eventTypes = response.data;
        });
    };

    retrieveEventTypes();
    retrieveEventFromService();

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.submitEventForm = function () {
        /* while compiling form , angular created this object*/
        $scope.fields.customerId = $scope.customerId;
        var data = $scope.fields;
        retrieveEventFromService();
        /* post to server*/
        EventFactory.createEvent(data).then(function (response) {
            $uibModalInstance.close();
        });
    };

    $scope.updateEventForm = function () {
        /* while compiling form , angular created this object*/
        var data = $scope.event;
        data.customerId = $scope.customerId;

        // update entity
        EventFactory.updateEvent(data).then(function (response) {
            $uibModalInstance.close();
        });
    };
};