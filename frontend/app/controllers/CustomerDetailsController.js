module.exports = function ($scope, $stateParams, $location, $uibModal, CustomerFactory, TagFactory) {

    var customerId = $stateParams.id;

    var getCustomerTags = function (customerId) {
        TagFactory.getAllCustomerTags(customerId).then(function (response) {
            $scope.tags = response.data;
        });
    };

    getCustomerTags(customerId);

    $scope.removeCustomerTag = function (customerId, tagName) {
        TagFactory.removeCustomerFromTag(customerId, tagName).then(function (response) {
            getCustomerTags(customerId);
        });
    };

    $scope.saveCustomerTag = function () {
        var newTag = $scope.newTag;
        if (newTag.length > 0) {
            TagFactory.addCustomerToTag(customerId, newTag).then(function (response) {
                getCustomerTags(customerId);
                $scope.newTag = '';
            });
        }
    };

    var getCustomerDetails = function (customerId) {
        CustomerFactory.getCustomer(customerId).then(function (response) {
            $scope.customerDetails = response.data;
        });
    };

    getCustomerDetails(customerId);

};