module.exports = function ($scope, EventTypeFactory, EventFactory) {


    var retrieveEvents = function () {
        EventFactory.getEvents().then(function (response) {
            $scope.eventList = response.data;
        });
    };

    retrieveEvents();
};