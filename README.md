# SPA CRM
## Opis
Projekt dotyczy zarządzania relacjami z klientami (ang. customer relationship management,  [_CRM_](https://pl.wikipedia.org/wiki/Zarz%C4%85dzanie_relacjami_z_klientami)),
projekt tworzony na bitbuckiecie, przy wykorzystaniu następujących technologii:
### Frontend 
* [ **AngularaJS.**](https://angularjs.org/)
* [ **HTML.**](https://pl.wikibooks.org/wiki/HTML)
* [ **NodeJs + NPM.**](https://nodejs.org/en/)
* [ **Webpack 2.**](https://webpack.github.io/)
### Backend
* [ **Java (z Maven).**](https://maven.apache.org/)
* [ **Spring (Boot, Data, Security).**](https://spring.io/)
* [ **JWT (Json Web Token).**](https://jwt.io)
## Możliwości
Stworzony system pozwala na: 
* Rejestrację użytkowników w systemie z uwzględnieniem  różnych typów ról. (administrator (admin), pracownik (user))

* Możliwość manipulacji ustawieniami użytkownika (hasła, dane kontaktowe itp.), oraz aktualizacji danych.
* Zapisywanie w systemie informacji o kontaktach - dane kontaktowe, grupowanie kontaktów za pomocą tagów.
* Zarządzanie zdarzeniami związanymi z kontaktami.
	* Pozwala na przypisywanie kontaktom zdarzeń z grupy zdefiniowanych w systemie zdarzeń 
	* Pozwala użytkownikowi definiować własne, niestandardowe typy zdarzeń.

## Komentarz
Wybrano własny backend, bo w sztuce robienia aplikacji SPA równie ważne jest poprawne tworzenie [**APIREST**](https://pl.wikipedia.org/wiki/Representational_State_Transfer)),

Aplikacja została zabezpieczona za pomocą technologii JWT. Oznacza to, że każde zapytanie wysyłane do API jest podpisane tokenem, dołączonym do zapytania w nagłówku Authorization.

Format nagłówka:

**Authorization: Bearer TUTAJ_WYGENEROWANY_TOKEN**


Skupiono się na rozróżnieniu ról na Administrator (rola admin), który posiada większe uprawnienia, np. może przeglądać wszystkie zgromadzone w systemie kontakty. Pracownik (user), może przeglądać tylko własne kontakty i tylko w obrębie nich może wykonywać działania, takie jak edycja klientów, dodawanie zdarzeń, tagów, notatek, raportów finansowych.
Adminstrator systemu może również wyświetlać listę wszystkich użytkowników systemu, oraz definiować typy zdarzeń.

## Instalacja aplikacji

W celu uruchomienia aplikacji należy posiadać:

 * Maven (wersja od 3.3.9)
 * NodeJs (wersja od 7.2)
 * NPM (wersja od 4.5.0)
 * Java JDK 1.8

Przed uruchomieniem aplikacji należy ją zbudować. W katalogu głównym projektu należy wpisać polecenia:

**mvn clean install -DskipTests**

**mvn package -DskipTests**

Uruchomić zbudowane archiwum .jar poleceniem:

**java -jar backend/target/backend-0.0.1-SNAPSHOT.jar**

Po uruchomieniu archiwum można korzystać z aplikacji pod adresem:
http://localhost:8090/index.html


## Autorzy
* Gać Konrad
* Piestrzyński Dominik
* Podmokły Michał