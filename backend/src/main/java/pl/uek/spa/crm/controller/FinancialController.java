package pl.uek.spa.crm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.uek.spa.crm.model.Financial;
import pl.uek.spa.crm.service.authentication.NotAuthorizedToPerformActionException;
import pl.uek.spa.crm.service.financial.FinancialNotFoundException;
import pl.uek.spa.crm.service.financial.FinancialService;


import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
public class FinancialController {


    @Autowired
    private FinancialService financialService;

    @GetMapping(value = "/api/financial/")
    private ResponseEntity<List<Financial>> getAllFinancial(){
        List<Financial> financial = financialService.getAllFinancial();
        if (financial.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(financial, HttpStatus.OK);

    }


    @GetMapping(value = "/api/financial/customer/{id}")
    private ResponseEntity<List<Financial>> getAllFinancialbyCustomerId(@PathVariable("id") Long id){
        List<Financial> financial = financialService.getAllFinancial();
        if (financial.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(financial, HttpStatus.OK);
    }



    @GetMapping(value = "/api/financial/{id}")
    private ResponseEntity<Financial> getFinancial(@PathVariable("id") long id){
        Optional<Financial> financial = null;
        try {
            financial = Optional.ofNullable(financialService.getFinancialById(id));
        }
        catch (NotAuthorizedToPerformActionException e){
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        if (financial.isPresent()) return new ResponseEntity<>(financial.get(), HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PostMapping(value = "/api/financial/")
    private ResponseEntity<Financial> createCustomer(@RequestBody Financial customer){
        Optional<Financial> savedFinancial = Optional.ofNullable(financialService.saveFinancial(customer));
        if (savedFinancial.isPresent()) return new ResponseEntity<>(savedFinancial.get(), HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PutMapping(value = "/api/financial/{id}")
    private ResponseEntity<Financial> updateFinancial(@PathVariable("id") long id, @RequestBody Financial financial){
        try {
            Financial updatedCustomer = financialService.updateFinancial(id, financial);
            return new ResponseEntity<>(updatedCustomer, HttpStatus.OK);
        } catch (FinancialNotFoundException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        catch (NotAuthorizedToPerformActionException e){
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @DeleteMapping(value = "/api/financial/{id}")
    private ResponseEntity<Void> deleteFinancial(@PathVariable("id") long id){
        try {
            financialService.deleteFinancial(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (FinancialNotFoundException e){
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }





}
