package pl.uek.spa.crm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.uek.spa.crm.model.event.EventType;
import pl.uek.spa.crm.service.event.EventService;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
public class EventTypeController {

    @Autowired
    private EventService eventService;
    
    @GetMapping(value = "/api/eventType/")
    private ResponseEntity<List<EventType>> getAllEventTypes(){
        List<EventType> eventTypes = eventService.findAllEventTypes();
        if (eventTypes.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(eventTypes, HttpStatus.OK);
    }

    @GetMapping(value = "/api/eventType/{id}")
    private ResponseEntity<EventType> getEventType(@PathVariable("id") long id){
        Optional<EventType> eventById = Optional.ofNullable(eventService.getTypeById(id));
        if (eventById.isPresent()) return new ResponseEntity<>(eventById.get(), HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/api/eventType/")
    private ResponseEntity<EventType> createEventType(@RequestBody EventType eventType){
        Optional<EventType> type = Optional.ofNullable(eventService.createEventType(eventType));
        if (type.isPresent()) return new ResponseEntity<>(type.get(), HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PutMapping(value = "/api/eventType/{id}")
    private ResponseEntity<EventType> updateEventType(@PathVariable("id") long id, @RequestBody EventType eventType){
        try {
            EventType updatedEventType = eventService.updateEventType(id, eventType);
            return new ResponseEntity<>(eventType, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/api/eventType/{id}")
    private ResponseEntity<Void> deleteEventType(@PathVariable("id") long id){
        try {
            eventService.deleteEventType(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }    
}
