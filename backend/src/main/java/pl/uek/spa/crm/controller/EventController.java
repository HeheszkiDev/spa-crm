package pl.uek.spa.crm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import pl.uek.spa.crm.model.event.Event;
import pl.uek.spa.crm.service.event.EventService;

import java.util.List;
import java.util.Optional;

@Controller
@CrossOrigin("*")
public class EventController {


    @Autowired
    private EventService eventService;

    @GetMapping(value = "/api/event/")
    private ResponseEntity<List<Event>> getAllEvents(){
        List<Event> events = eventService.findAllEvents();
        if (events.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(events, HttpStatus.OK);
    }

    @GetMapping(value = "/api/event/customer/{id}")
    private ResponseEntity<List<Event>> getAllEvents(@PathVariable("id") long id){
        List<Event> events = eventService.findAllEventsByCustomerId(id);
        if (events.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(events, HttpStatus.OK);
    }

    @GetMapping(value = "/api/event/{id}")
    private ResponseEntity<Event> getEvent(@PathVariable("id") long id){
        Optional<Event> eventById = Optional.ofNullable(eventService.getEventById(id));
        if (eventById.isPresent()) return new ResponseEntity<>(eventById.get(), HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/api/event/")
    private ResponseEntity<Event> createEvent(@RequestBody Event event){
        try {
            Event savedEvent = eventService.createEvent(event);
            return new ResponseEntity<>(savedEvent, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/api/event/{id}")
    private ResponseEntity<Event> updateEvent(@PathVariable("id") long id, @RequestBody Event event){
        try {
            Event updatedEvent = eventService.updateEvent(id, event);
            return new ResponseEntity<>(updatedEvent, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/api/event/{id}")
    private ResponseEntity<Void> deleteEventType(@PathVariable("id") long id){
        try {
            eventService.deleteEvent(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
