package pl.uek.spa.crm.service.tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.spa.crm.model.Customer;
import pl.uek.spa.crm.model.Tag;
import pl.uek.spa.crm.model.UserAccount;
import pl.uek.spa.crm.repository.customer.CustomerRepository;
import pl.uek.spa.crm.repository.tag.TagRepository;
import pl.uek.spa.crm.service.authentication.AuthenticationService;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TagServiceImpl implements TagService{

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public List<Tag> getAllTags() {
        UserAccount userAccount = authenticationService.userAccountDetails();
        if (userAccount.hasAdminRole()) return tagRepository.findAll();
        return tagRepository.findAllByCreatorId(userAccount.getId());
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public void deleteTag(long tagId) {
        tagRepository.delete(tagId);
    }

    @Override
    @PreAuthorize("hasRole('ROLE_USER')")
    public void addCustomerToTag(Long customerId, String tagName) {
        if (customerRepository.exists(customerId)){
            Tag retrievedTag = tagRepository.findTagByTagName(tagName);
            if (retrievedTag!=null){
                retrievedTag.getCustomersId().add(customerId);
                tagRepository.save(retrievedTag);
            }
            else {
                Set<Long> set = new HashSet<>();
                set.add(customerId);
                tagRepository.save(
                        new Tag(tagName,
                                authenticationService.userAccountDetails().getId(), set));
            }
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_USER')")
    @Transactional
    public void removeCustomerFromTag(Long customerId, String tagName) {
        if (customerRepository.exists(customerId)){
            Tag retrievedTag = tagRepository.findTagByTagName(tagName);
            if (retrievedTag!=null){
                retrievedTag.getCustomersId().remove(customerId);
                tagRepository.save(retrievedTag);
            }
            else {
                System.out.println("Tag doesn't exist " + tagName);
            }
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_USER')")
    @Transactional
    public List<Customer> findCustomersFromTag(String tagName) {
        Tag tagByTagName = tagRepository.findTagByTagName(tagName);
        if (tagByTagName==null) return Collections.emptyList();
        return customerRepository.findAllByIdIn(tagByTagName.getCustomersId());
    }

    @Override
    @PreAuthorize("hasRole('ROLE_USER')")
    @Transactional
    public List<Tag> findCustomerTags(Long customerId) {
        return tagRepository.findAllByCustomersIdIsContaining(customerId);
    }
}
