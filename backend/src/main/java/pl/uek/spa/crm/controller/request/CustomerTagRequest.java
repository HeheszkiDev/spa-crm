package pl.uek.spa.crm.controller.request;

public class CustomerTagRequest {

    private Long customerId;
    private String tagName;

    public CustomerTagRequest() {
    }

    public CustomerTagRequest(Long customerId, String tagName) {
        this.customerId = customerId;
        this.tagName = tagName;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
