package pl.uek.spa.crm.service.user.account;

import org.springframework.security.access.prepost.PreAuthorize;
import pl.uek.spa.crm.controller.request.UpdatePasswordRequest;
import pl.uek.spa.crm.model.UserAccount;

import java.util.List;

public interface UserAccountService {

    List<UserAccount> users();
    UserAccount userById(Long id);
    UserAccount deleteUser(Long id);
    UserAccount createUser(UserAccount userAccount);
    UserAccount updateUser(UserAccount userAccount);
    boolean updateUserData(UserAccount userAccount);
    boolean changePassword(UpdatePasswordRequest request);
}
