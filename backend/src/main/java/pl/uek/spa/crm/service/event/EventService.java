package pl.uek.spa.crm.service.event;

import pl.uek.spa.crm.model.event.Event;
import pl.uek.spa.crm.model.event.EventType;

import java.util.List;

public interface EventService {

    EventType createEventType(EventType eventType);
    EventType updateEventType(long id, EventType eventType);
    EventType getTypeById(long id);
    List<EventType> findAllEventTypes();
    void deleteEventType(long id);
    Event createEvent(Event event) throws EventTypeNotExistException;
    Event updateEvent(long id, Event event) throws EventTypeNotExistException, EventNotExistException;
    Event getEventById(long id);
    List<Event> findAllEvents();
    List<Event> findAllEventsByCustomerId(long customerId);
    void deleteEvent(long id) throws EventNotExistException;
}
