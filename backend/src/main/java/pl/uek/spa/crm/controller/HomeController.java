package pl.uek.spa.crm.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.uek.spa.crm.controller.request.AuthRequest;
import pl.uek.spa.crm.model.UserAccount;
import pl.uek.spa.crm.service.authentication.AuthenticationService;
import pl.uek.spa.crm.service.dashboard.DashboardService;
import pl.uek.spa.crm.service.dashboard.DashboardStats;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@RestController
@CrossOrigin(value = "*")
public class HomeController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private DashboardService dashboardService;

    @PostMapping(value = "/register")
    public ResponseEntity<UserAccount> createUser(@RequestBody UserAccount newUser) {
        return new ResponseEntity<>(authenticationService.createUserAccount(newUser), HttpStatus.CREATED);
    }

    @RequestMapping("/user")
    public ResponseEntity<UserAccount> user() {
        return new ResponseEntity<UserAccount>(authenticationService.userAccountDetails(), HttpStatus.OK);
    }

    @PostMapping(value = "/authenticate")
    public ResponseEntity<Map<String, Object>> login(@RequestBody AuthRequest request) throws IOException {
        Map<String, Object> tokenMap = authenticationService.login(request.getUsername(), request.getPassword());
        if (tokenMap.get("token")==null){
            return new ResponseEntity<>(tokenMap, HttpStatus.UNAUTHORIZED);
        }
        else {
            return new ResponseEntity<>(tokenMap, HttpStatus.OK);
        }
    }

    @GetMapping(value = "/api/stats")
    public ResponseEntity<DashboardStats> dashboardStats() throws IOException {
        return new ResponseEntity<>(dashboardService.getStats(), HttpStatus.OK);
    }

}