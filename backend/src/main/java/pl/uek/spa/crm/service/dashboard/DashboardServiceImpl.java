package pl.uek.spa.crm.service.dashboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.spa.crm.model.UserAccount;
import pl.uek.spa.crm.repository.customer.CustomerRepository;
import pl.uek.spa.crm.repository.event.EventRepository;
import pl.uek.spa.crm.repository.financial.FinancialRepository;
import pl.uek.spa.crm.repository.note.NoteRepository;
import pl.uek.spa.crm.service.authentication.AuthenticationService;

@Service
public class DashboardServiceImpl implements DashboardService{

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private NoteRepository noteRepository;

    @Autowired
    private FinancialRepository financialRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    @Transactional
    public DashboardStats getStats() {
        UserAccount userAccount = authenticationService.userAccountDetails();
        if (userAccount.hasAdminRole()){
            return new DashboardStats(customerRepository.count(), eventRepository.count(), noteRepository.count(), financialRepository.count());
        }
        Long ownerId = userAccount.getId();
        return new DashboardStats(customerRepository.countByOwnerId(ownerId), eventRepository.countByCreatorId(ownerId), noteRepository.countByCreatorId(ownerId), financialRepository.countByCreatorId(ownerId));
    }
}
