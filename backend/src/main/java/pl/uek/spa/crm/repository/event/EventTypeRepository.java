package pl.uek.spa.crm.repository.event;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.uek.spa.crm.model.event.EventType;

import java.util.List;

@Repository
public interface EventTypeRepository extends JpaRepository<EventType, Long> {

    List<EventType> findAllByDeletedFalse();
}
