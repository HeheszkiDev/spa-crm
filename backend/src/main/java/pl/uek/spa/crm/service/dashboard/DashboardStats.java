package pl.uek.spa.crm.service.dashboard;

public class DashboardStats {

    private long customers;
    private long events;
    private long notes;
    private long reports;

    public DashboardStats() {
    }

    public DashboardStats(long customers, long events, long notes, long reports) {
        this.customers = customers;
        this.events = events;
        this.notes = notes;
        this.reports = reports;
    }

    public long getCustomers() {
        return customers;
    }

    public void setCustomers(long customers) {
        this.customers = customers;
    }

    public long getEvents() {
        return events;
    }

    public void setEvents(long events) {
        this.events = events;
    }

    public long getNotes() {
        return notes;
    }

    public void setNotes(long notes) {
        this.notes = notes;
    }

    public long getReports() {
        return reports;
    }

    public void setReports(long reports) {
        this.reports = reports;
    }
}
