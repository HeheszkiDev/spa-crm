package pl.uek.spa.crm.service.customer;

import pl.uek.spa.crm.model.Customer;
import pl.uek.spa.crm.service.authentication.NotAuthorizedToPerformActionException;

import java.util.List;

public interface CustomerService {

    Customer saveCustomer(Customer customer);
    void deleteCustomer(long id) throws CustomerNotFoundException, NotAuthorizedToPerformActionException;
    Customer updateCustomer(long id, Customer customer) throws CustomerNotFoundException, NotAuthorizedToPerformActionException;
    List<Customer> getAllCustomers();
    List<Customer> findAllCustomersByOwnerId(long id);
    Customer getCustomerById(long id);
}
