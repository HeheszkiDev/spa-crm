package pl.uek.spa.crm.service.dashboard;

public interface DashboardService {

    DashboardStats getStats();
}
