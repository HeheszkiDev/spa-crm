package pl.uek.spa.crm.service.user.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.spa.crm.controller.request.UpdatePasswordRequest;
import pl.uek.spa.crm.model.UserAccount;
import pl.uek.spa.crm.repository.user.UserAccountRepository;
import pl.uek.spa.crm.service.authentication.AuthenticationService;

import java.util.List;

@Service
public class UserAccountServiceImpl implements UserAccountService{

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<UserAccount> users() {
        return userAccountRepository.findAll();
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserAccount userById(Long id) {
        return userAccountRepository.findOne(id);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserAccount deleteUser(Long id) {
        UserAccount userAccount = userAccountRepository.findOne(id);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String loggedUsername = auth.getName();
        if (userAccount == null) {
            return null;
        } else if (userAccount.getUsername().equalsIgnoreCase(loggedUsername)) {
            throw new RuntimeException("You cannot delete your account");
        } else {
            userAccountRepository.delete(userAccount);
            return userAccount;
        }


    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserAccount createUser(UserAccount userAccount) {
        if (userAccountRepository.findOneByUsername(userAccount.getUsername()) != null) {
            throw new RuntimeException("Username already exist");
        }
        return userAccountRepository.save(userAccount);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserAccount updateUser(UserAccount userAccount) {
        if (userAccountRepository.findOneByUsername(userAccount.getUsername()) != null
                && userAccountRepository.findOneByUsername(userAccount.getUsername()).getId() != userAccount.getId()) {
            throw new RuntimeException("Username already exist");
        }
        return userAccount;
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public boolean updateUserData(UserAccount userAccount) {
        UserAccount byUsername = userAccountRepository.findOneByUsername(userAccount.getUsername());
        if (byUsername.getPassword().equals(userAccount.getPassword())){
            byUsername.setFirstName(userAccount.getFirstName());
            byUsername.setLastName(userAccount.getLastName());
            byUsername.setEmail(userAccount.getEmail());
            userAccountRepository.save(byUsername);
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public boolean changePassword(UpdatePasswordRequest request) {
        UserAccount byUsername = userAccountRepository.findOneByUsername(request.getUsername());
        if (byUsername.getPassword().equals(request.getPassword())){
            byUsername.setPassword(request.getNewPassword());
            userAccountRepository.save(byUsername);
            return true;
        }
        return false;
    }
}
