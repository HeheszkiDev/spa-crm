package pl.uek.spa.crm.service.authentication;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.spa.crm.model.UserAccount;
import pl.uek.spa.crm.repository.user.UserAccountRepository;

import java.util.*;

@Service
public class AuthenticationServiceImpl implements AuthenticationService{

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Override
    @Transactional
    public UserAccount createUserAccount(UserAccount newUser) {
        if (userAccountRepository.findOneByUsername(newUser.getUsername()) != null) {
            throw new RuntimeException("Username already exist");
        }
        List<String> roles = new ArrayList<>();
        roles.add("ROLE_USER");
        newUser.setRoles(roles);
        return userAccountRepository.save(newUser);
    }

    @Override
    @Transactional
    public UserAccount userAccountDetails() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String loggedUsername = auth.getName();
        return userAccountRepository.findOneByUsername(loggedUsername);
    }

    @Override
    @Transactional
    public Map<String, Object> login(String username, String password) {
        String token = null;
        Optional<UserAccount> userAccount = Optional.ofNullable(userAccountRepository.findOneByUsername(username));
        Map<String, Object> tokenMap = new HashMap<>();
        if (authorized(password, userAccount)){
            token = Jwts.builder().setSubject(username).claim("roles", userAccount.get().getRoles()).setIssuedAt(new Date())
                    .signWith(SignatureAlgorithm.HS256, "secretkey").compact();
            tokenMap.put("token", token);
            tokenMap.put("user", userAccount);
        }
        else {
            tokenMap.put("token", null);
        }
        return tokenMap;
    }

    private boolean authorized(String password, Optional<UserAccount> userAccount) {
        return userAccount.isPresent() && userAccount.get().getPassword().equals(password);
    }
}
