package pl.uek.spa.crm.service.note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.spa.crm.model.Note;
import pl.uek.spa.crm.model.UserAccount;
import pl.uek.spa.crm.repository.customer.CustomerRepository;
import pl.uek.spa.crm.repository.note.NoteRepository;
import pl.uek.spa.crm.service.authentication.AuthenticationService;
import pl.uek.spa.crm.service.authentication.NotAuthorizedToPerformActionException;

import java.util.Date;
import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {

    @Autowired
    private NoteRepository noteRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public Note saveNote(Note note) {
        UserAccount userAccount = authenticationService.userAccountDetails();
        note.setCreatorId(userAccount.getId());
        note.setCreatedDate(new Date());
        return noteRepository.save(note);
    }


    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public void deleteNote(long id) throws NoteNotFoundException {
        if (!noteRepository.exists(id)) throw new NoteNotFoundException();
        noteRepository.delete(id);
    }


    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public Note updateNote(long id, Note note) throws NoteNotFoundException, NotAuthorizedToPerformActionException {
        UserAccount userAccount = authenticationService.userAccountDetails();
        Note retrievedNote = noteRepository.findOne(note.getId());
          if (retrievedNote==null) throw new NoteNotFoundException();
//          if (isNotCreatorOrAdmin(userAccount, retrievedNote)) throw new NotAuthorizedToPerformActionException();
        retrievedNote.setTitle(note.getTitle());
        retrievedNote.setDescription(note.getDescription());
        retrievedNote.setCustomerId(note.getCustomerId());

        return noteRepository.save(retrievedNote);
    }

    private boolean isNotCreatorOrAdmin(UserAccount userAccount, Note retrievedNote) {
        return !retrievedNote.getCreatorId().equals(userAccount.getId()) || !userAccount.hasRole("ROLE_ADMIN");
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public List<Note> getAllNote() {
        UserAccount userAccount = authenticationService.userAccountDetails();
        if (userAccount.hasRole("ROLE_ADMIN")) return noteRepository.findAll();
        return noteRepository.findAllByCreatorIdOrderByCreatedDateDesc(userAccount.getId());
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public Note getNoteById(long id) throws NotAuthorizedToPerformActionException {
        Note retrievedNote = noteRepository.getOne(id);
        if (isNotCreatorOrAdmin(authenticationService.userAccountDetails(), retrievedNote)) throw new NotAuthorizedToPerformActionException();
        return retrievedNote;
    }







}
