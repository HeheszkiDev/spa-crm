package pl.uek.spa.crm.repository.financial;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.uek.spa.crm.model.Financial;

import java.util.List;

/**
 * Created by Tochi on 2017-05-10.
 */
@Repository
public interface FinancialRepository extends JpaRepository<Financial, Long>, FinancialRepositoryCustom{

    List<Financial> findAllByCreatorIdOrderByCreatedDateDesc(long id);
    Integer countByCreatorId(Long creatorId);

}


