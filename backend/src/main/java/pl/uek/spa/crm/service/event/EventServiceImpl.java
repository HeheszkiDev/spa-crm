package pl.uek.spa.crm.service.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.spa.crm.model.UserAccount;
import pl.uek.spa.crm.model.event.Event;
import pl.uek.spa.crm.model.event.EventType;
import pl.uek.spa.crm.repository.event.EventRepository;
import pl.uek.spa.crm.repository.event.EventTypeRepository;
import pl.uek.spa.crm.service.authentication.AuthenticationService;

import java.util.Date;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventTypeRepository eventTypeRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private AuthenticationService authenticationService;

    private final Logger LOG = LoggerFactory.getLogger(this.getClass().getName());

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public EventType createEventType(EventType eventType) {
        return eventTypeRepository.save(eventType);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public EventType updateEventType(long id, EventType eventType) {
        EventType persisted = eventTypeRepository.getOne(id);
        persisted.setEventName(eventType.getEventName());
        return eventTypeRepository.save(persisted);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public EventType getTypeById(long id) {
        return eventTypeRepository.getOne(id);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public List<EventType> findAllEventTypes() {
        return eventTypeRepository.findAllByDeletedFalse();
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteEventType(long id) {
        EventType toRemove = eventTypeRepository.findOne(id);
        toRemove.setDeleted(true);
        eventTypeRepository.save(toRemove);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public Event createEvent(Event event) throws EventTypeNotExistException {
        EventType eventType = event.getEventType();
        if (!eventTypeRepository.exists(eventType.getId())) throw new EventTypeNotExistException();
        EventType retrievedEventType = eventTypeRepository.findOne(eventType.getId());
        event.setEventType(retrievedEventType);
        event.setCreatorId(authenticationService.userAccountDetails().getId());
        return eventRepository.save(event);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public Event updateEvent(long id, Event event) throws EventTypeNotExistException, EventNotExistException {
        EventType eventType = event.getEventType();
        if (!eventTypeRepository.exists(eventType.getId())) {
            LOG.error("Event type not exists. EventTypeId: " + id);
            throw new EventTypeNotExistException();
        }
        if (!eventRepository.exists(id)) {
            LOG.error("Event not exists. EventId: " + id);
            throw new EventNotExistException();
        }

        Event persisted = eventRepository.findOne(id);
        EventType persistedType = eventTypeRepository.findOne(event.getEventType().getId());
        persisted.setCustomerId(event.getCustomerId());
        persisted.setDescription(event.getDescription());
        persisted.setEventDate(event.getEventDate());
        persisted.setEventType(persistedType);
        persisted.setName(event.getName());
        return eventRepository.save(persisted);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public Event getEventById(long id) {
        return eventRepository.getOne(id);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public List<Event> findAllEvents() {
        UserAccount userAccount = authenticationService.userAccountDetails();
        if (userAccount.hasAdminRole()) return eventRepository.findAll();
        return eventRepository.findAllByCreatorIdAndDeletedFalseOrderByEventDateDesc(userAccount.getId());
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public List<Event> findAllEventsByCustomerId(long customerId) {
        return eventRepository.findAllByCustomerIdAndDeletedFalseOrderByEventDateDesc(customerId);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public void deleteEvent(long id) throws EventNotExistException {
        if (!eventRepository.exists(id)) {
            throw new EventNotExistException();
        }
        Event toRemove = eventRepository.getOne(id);
        toRemove.setDeleted(true);
        eventRepository.save(toRemove);
    }
}
