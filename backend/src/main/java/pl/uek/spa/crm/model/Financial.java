package pl.uek.spa.crm.model;

import javax.persistence.*;
import javax.persistence.GenerationType;
import java.util.Date;

/**
 * Created by Tochi on 2017-05-10.
 */
@Entity
public class Financial {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title="none";
    private String description;
    private double amount;

    private Long customerId;
    private Date createdDate;
    private Long creatorId;

    public Financial(){
    }

    public Financial(String title, String description,double amount, Long customerId, Date createdDate, Long creatorId) {
        this.title = title;
        this.description = description;
        this.amount = amount;
        this.customerId = customerId;
        this.createdDate = createdDate;
        this.creatorId = creatorId;
    }


    public Long getId() { return id;}

    public void setId(Long id) {this.id = id;}

    public String getTitle() {return title;}

    public void setTitle(String title) {this.title = title;}

    public String getDescription() {return description;}

    public void setDescription(String description) {this.description = description;}

    public double getAmount() {return amount;}

    public void setAmount(double amount) {this.amount = amount;}

    public Long getCustomerId() {return customerId;}

    public void setCustomerId(Long customerId) {this.customerId = customerId;}

    public Date getCreatedDate() {return createdDate;}

    public void setCreatedDate(Date createdDate) {this.createdDate = createdDate;}

    public Long getCreatorId() {return creatorId;}

    public void setCreatorId(Long creatorId) {this.creatorId = creatorId;}
}
