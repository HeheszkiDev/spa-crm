package pl.uek.spa.crm.model;


import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String tagName;
    private Long creatorId;
    @ElementCollection
    private Set<Long> customersId;

    public Tag() {
    }

    public Tag(String tagName, Long creatorId, Set<Long> customersId) {
        this.tagName = tagName;
        this.creatorId = creatorId;
        this.customersId = customersId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Set<Long> getCustomersId() {
        return customersId;
    }

    public void setCustomersId(Set<Long> customersId) {
        this.customersId = customersId;
    }
}
