package pl.uek.spa.crm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.uek.spa.crm.model.Note;
import pl.uek.spa.crm.service.authentication.NotAuthorizedToPerformActionException;
import pl.uek.spa.crm.service.note.NoteNotFoundException;
import pl.uek.spa.crm.service.note.NoteService;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
public class NoteController {


    @Autowired
    private NoteService noteService;

    @GetMapping(value = "/api/note/")
    private ResponseEntity<List<Note>> getAllNote(){
        List<Note> note = noteService.getAllNote();
        if (note.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(note, HttpStatus.OK);

    }


    @GetMapping(value = "/api/note/customer/{id}")
    private ResponseEntity<List<Note>> getAllNotebyCustomerId(@PathVariable("id") Long id){
        List<Note> note = noteService.getAllNote();
        if (note.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(note, HttpStatus.OK);

    }





    @GetMapping(value = "/api/note/{id}")
    private ResponseEntity<Note> getNote(@PathVariable("id") long id){
        Optional<Note> note = null;
        try {
            note = Optional.ofNullable(noteService.getNoteById(id));
        }
        catch (NotAuthorizedToPerformActionException e){
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        if (note.isPresent()) return new ResponseEntity<>(note.get(), HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/api/note/")
    private ResponseEntity<Note> createCustomer(@RequestBody Note customer){
        Optional<Note> savedNote = Optional.ofNullable(noteService.saveNote(customer));
        if (savedNote.isPresent()) return new ResponseEntity<>(savedNote.get(), HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PutMapping(value = "/api/note/{id}")
    private ResponseEntity<Note> updateNote(@PathVariable("id") long id, @RequestBody Note note){
        try {
            Note updatedCustomer = noteService.updateNote(id, note);
            return new ResponseEntity<>(updatedCustomer, HttpStatus.OK);
        } catch (NoteNotFoundException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        catch (NotAuthorizedToPerformActionException e){
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @DeleteMapping(value = "/api/note/{id}")
    private ResponseEntity<Void> deleteNote(@PathVariable("id") long id){
        try {
            noteService.deleteNote(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (NoteNotFoundException e){
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }





}
