package pl.uek.spa.crm.repository.event;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.uek.spa.crm.model.event.Event;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long>, EventRepositoryCustom {

    List<Event> findAllByCustomerIdOrderByEventDateDesc(long customerId);
    List<Event> findAllByCustomerIdAndDeletedFalseOrderByEventDateDesc(long customerId);
    List<Event> findAllByCreatorIdAndDeletedFalseOrderByEventDateDesc(long creatorId);
    Integer countByCreatorId(long creatorId);
}
