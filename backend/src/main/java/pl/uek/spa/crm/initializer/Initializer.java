package pl.uek.spa.crm.initializer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.spa.crm.model.Address;
import pl.uek.spa.crm.model.Customer;
import pl.uek.spa.crm.model.Note;
import pl.uek.spa.crm.model.UserAccount;
import pl.uek.spa.crm.model.event.Event;
import pl.uek.spa.crm.model.event.EventType;
import pl.uek.spa.crm.repository.customer.CustomerRepository;
import pl.uek.spa.crm.repository.event.EventRepository;
import pl.uek.spa.crm.repository.event.EventTypeRepository;
import pl.uek.spa.crm.repository.note.NoteRepository;
import pl.uek.spa.crm.repository.user.UserAccountRepository;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Date;

@Component
public class Initializer {

    @Autowired
    private UserAccountRepository repository;

    @Autowired
    private EventTypeRepository typeRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private NoteRepository noteRepository;

    @PostConstruct
    @Transactional
    public void init() {
            /* To remove after first development phase */
        UserAccount devAdminAccount = new UserAccount();
        devAdminAccount.setLastName("admin");
        devAdminAccount.setFirstName("admin");
        devAdminAccount.setUsername("admin");
        devAdminAccount.setEmail("admin@spa.crm.com");
        devAdminAccount.setPassword("zaq12wsx");
        devAdminAccount.setRoles(Arrays.asList("ROLE_USER", "ROLE_ADMIN"));
        repository.save(devAdminAccount);

        UserAccount devUserAccount = new UserAccount();
        devUserAccount.setLastName("Smith");
        devUserAccount.setFirstName("John");
        devUserAccount.setEmail("john@smith.com");
        devUserAccount.setUsername("test");
        devUserAccount.setPassword("test");
        devUserAccount.setRoles(Arrays.asList("ROLE_USER"));
        UserAccount userAccount = repository.save(devUserAccount);

        typeRepository.save(new EventType("Phone call"));
        typeRepository.save(new EventType("Meeting"));
        typeRepository.save(new EventType("Webinar"));
        EventType emailType = typeRepository.save(new EventType("E-mail"));

        Address annaNowakAddress = new Address();
        annaNowakAddress.setCity("Kraków");
        annaNowakAddress.setCountry("Poland");
        annaNowakAddress.setStreet("Rakowicka 27");
        annaNowakAddress.setRegion("Małopolska");
        annaNowakAddress.setZipCode("31-510");
        Customer annaNowak = new Customer("Anna", "Nowak", annaNowakAddress, "12 293 57 00", "anna.nowak.example@uek.krakow.pl", userAccount.getId());
        Customer persistedAnna = customerRepository.save(annaNowak);

        Address johnCarmackAddress = new Address();
        johnCarmackAddress.setCity("Roeland Park");
        johnCarmackAddress.setCountry("USA");
        johnCarmackAddress.setStreet("Mission Road");
        johnCarmackAddress.setRegion("Kansas");
        johnCarmackAddress.setZipCode("KS 66205");
        Customer johnCarmack = new Customer("John", "Carmack", johnCarmackAddress, "", "john@carmack.com", userAccount.getId());
        Customer persistedCarmack = customerRepository.save(johnCarmack);

        Note note = new Note("Doom creator", "John D. Carmack is a well known video game creator. He was one of the creators of id Software, a video game company, in 1991. Carmack was the main programmer of the first-person shooter games Wolfenstein 3D, Doom, Quake, and their sequels.",
                persistedCarmack.getId(), new Date(), userAccount.getId());

        noteRepository.save(note);

        Event newsletterEvent = new Event(persistedAnna.getId(), new EventType("Newsletter"), "Newsletter email", "Newsletter e-mail with latest products offer has been sent to customer", new Date(), userAccount.getId());
        eventRepository.save(newsletterEvent);
    }
}

