package pl.uek.spa.crm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.uek.spa.crm.controller.request.CustomerTagRequest;
import pl.uek.spa.crm.controller.request.CustomersFromTagRequest;
import pl.uek.spa.crm.model.Customer;
import pl.uek.spa.crm.model.Tag;
import pl.uek.spa.crm.service.tag.TagService;

import java.util.List;

@RestController
@CrossOrigin(value = "*")
public class TagController {

    @Autowired
    private TagService tagService;

    @GetMapping(value = "/api/tag/")
    private ResponseEntity<List<Tag>> getAllTags(){
        List<Tag> tags = tagService.getAllTags();
        if (tags.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(tags, HttpStatus.OK);
    }

    @GetMapping(value = "/api/tag/customer/{customerId}")
    private ResponseEntity<List<Tag>> getAllCustomerTags(@PathVariable("customerId") Long customerId){
        List<Tag> tags = tagService.findCustomerTags(customerId);
        if (tags.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(tags, HttpStatus.OK);
    }

    @GetMapping(value = "/api/tag/customer")
    private ResponseEntity<List<Customer>> getAllCustomersFromTag(@RequestBody CustomersFromTagRequest request){
        List<Customer> customers = tagService.findCustomersFromTag(request.getTagName());
        if (customers.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @GetMapping(value = "/api/tag/{tag}")
    private ResponseEntity<List<Customer>> getCustomersFromTag(@PathVariable("tag") String tag){
        List<Customer> customers = tagService.findCustomersFromTag(tag);
        if (customers.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @PostMapping(value = "/api/tag/customer")
    private ResponseEntity<Void> addCustomerToTag(@RequestBody CustomerTagRequest request){
        tagService.addCustomerToTag(request.getCustomerId(), request.getTagName());
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @PostMapping(value = "/api/tag/customer/remove")
    private ResponseEntity<Void> removeCustomerFromTag(@RequestBody CustomerTagRequest request){
        tagService.removeCustomerFromTag(request.getCustomerId(), request.getTagName());
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @DeleteMapping(value = "/api/tag/{id}")
    private ResponseEntity<Void> removeTag(@PathVariable("id") Long id){
        tagService.deleteTag(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
