package pl.uek.spa.crm.service.financial;


import pl.uek.spa.crm.model.Financial;
import pl.uek.spa.crm.service.authentication.NotAuthorizedToPerformActionException;


import java.util.List;

/**
 * Created by Tochi on 2017-04-30.
 */
public interface FinancialService {
    Financial saveFinancial(Financial financial);

    void deleteFinancial(long id) throws FinancialNotFoundException;

    Financial updateFinancial(long id, Financial financial) throws FinancialNotFoundException, NotAuthorizedToPerformActionException;

    List<Financial> getAllFinancial();

    Financial getFinancialById(long id) throws NotAuthorizedToPerformActionException;


}
