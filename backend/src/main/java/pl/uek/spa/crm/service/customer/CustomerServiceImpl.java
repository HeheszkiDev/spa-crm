package pl.uek.spa.crm.service.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.spa.crm.model.Address;
import pl.uek.spa.crm.model.Customer;
import pl.uek.spa.crm.model.UserAccount;
import pl.uek.spa.crm.repository.customer.CustomerRepository;
import pl.uek.spa.crm.service.authentication.AuthenticationService;
import pl.uek.spa.crm.service.authentication.NotAuthorizedToPerformActionException;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService{

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public Customer saveCustomer(Customer customer) {
        UserAccount userAccount = authenticationService.userAccountDetails();
        customer.setOwnerId(userAccount.getId());
        return customerRepository.save(customer);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public Customer updateCustomer(long id, Customer customer) throws CustomerNotFoundException, NotAuthorizedToPerformActionException {
        UserAccount userAccount = authenticationService.userAccountDetails();
        Customer retrievedCustomer = customerRepository.getOne(id);
        if (retrievedCustomer==null) throw new CustomerNotFoundException();
        if (!retrievedCustomer.getOwnerId().equals(userAccount.getId())) throw new NotAuthorizedToPerformActionException();
        Address address = retrievedCustomer.getAddress();
        address.setCity(customer.getAddress().getCity());
        address.setCity(customer.getAddress().getStreet());
        address.setCity(customer.getAddress().getCountry());
        address.setCity(customer.getAddress().getRegion());
        address.setCity(customer.getAddress().getZipCode());
        retrievedCustomer.setFirstName(customer.getFirstName());
        retrievedCustomer.setLastName(customer.getLastName());
        retrievedCustomer.setEmail(customer.getEmail());
        retrievedCustomer.setPhone(customer.getPhone());
        return customerRepository.save(retrievedCustomer);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public void deleteCustomer(long id) throws CustomerNotFoundException, NotAuthorizedToPerformActionException {
        if (!customerRepository.exists(id)) throw new CustomerNotFoundException();
        UserAccount userAccount = authenticationService.userAccountDetails();
        Customer retrievedCustomer = customerRepository.getOne(id);
        if (!retrievedCustomer.getOwnerId().equals(userAccount.getId())) throw new NotAuthorizedToPerformActionException();
        customerRepository.delete(id);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public List<Customer> getAllCustomers() {
        UserAccount userAccount = authenticationService.userAccountDetails();
        if (userAccount.hasAdminRole()) return customerRepository.findAll();
        return customerRepository.findAllByOwnerId(userAccount.getId());
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Customer> findAllCustomersByOwnerId(long id) {
        return customerRepository.findAllByOwnerId(id);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public Customer getCustomerById(long id) {
        return customerRepository.findOne(id);
    }
}
