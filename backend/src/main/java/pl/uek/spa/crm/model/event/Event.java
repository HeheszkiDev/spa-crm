package pl.uek.spa.crm.model.event;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.Date;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private long customerId;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private EventType eventType;
    @Column(nullable = false)
    private String name;
    private String description;
    private Date eventDate;
    private boolean deleted;
    private Long creatorId;

    public Event() {
    }

    public Event(long customerId, EventType eventType, String name, String description, Date eventDate, Long creatorId) {
        this.customerId = customerId;
        this.eventType = eventType;
        this.name = name;
        this.description = description;
        this.eventDate = eventDate;
        this.creatorId = creatorId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public EventType getEventType() {
        return eventType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", customerId=" + customerId +
                ", eventType=" + eventType +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", eventDate=" + eventDate +
                ", deleted=" + deleted +
                ", creatorId=" + creatorId +
                '}';
    }
}
