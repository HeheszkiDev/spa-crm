package pl.uek.spa.crm.controller.request;

public class CustomersFromTagRequest {

    private String tagName;

    public CustomersFromTagRequest(String tagName) {
        this.tagName = tagName;
    }


    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}

