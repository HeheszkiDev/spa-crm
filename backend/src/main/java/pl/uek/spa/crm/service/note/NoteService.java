package pl.uek.spa.crm.service.note;

import pl.uek.spa.crm.model.Note;
import pl.uek.spa.crm.service.authentication.NotAuthorizedToPerformActionException;

import java.util.List;

/**
 * Created by Tochi on 2017-04-30.
 */
public interface NoteService {
    Note saveNote(Note note);

    void deleteNote(long id) throws NoteNotFoundException;

    Note updateNote(long id, Note note) throws NoteNotFoundException, NotAuthorizedToPerformActionException;

    List<Note> getAllNote();

    Note getNoteById(long id) throws NotAuthorizedToPerformActionException;


}
