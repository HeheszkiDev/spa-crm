package pl.uek.spa.crm.service.financial;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.spa.crm.model.Financial;
import pl.uek.spa.crm.model.UserAccount;
import pl.uek.spa.crm.repository.customer.CustomerRepository;
import pl.uek.spa.crm.repository.financial.FinancialRepository;
import pl.uek.spa.crm.service.authentication.AuthenticationService;
import pl.uek.spa.crm.service.authentication.NotAuthorizedToPerformActionException;


import java.util.Date;
import java.util.List;

/**
 * Created by Tochi on 2017-05-10.
 */
@Service
public class FinancialServiceImpl implements FinancialService {

    @Autowired
    private FinancialRepository financialRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public Financial saveFinancial(Financial financial) {
        UserAccount userAccount = authenticationService.userAccountDetails();
        financial.setCreatorId(userAccount.getId());
        financial.setCreatedDate(new Date());
        return financialRepository.save(financial);
    }


    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public void deleteFinancial(long id) throws FinancialNotFoundException {
        if (!financialRepository.exists(id)) throw new FinancialNotFoundException();
        financialRepository.delete(id);
    }


    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public Financial updateFinancial(long id, Financial financial) throws FinancialNotFoundException, NotAuthorizedToPerformActionException {
        UserAccount userAccount = authenticationService.userAccountDetails();
        Financial retrievedFinancial = financialRepository.findOne(financial.getId());
        if (retrievedFinancial==null) throw new FinancialNotFoundException();
//          if (isNotCreatorOrAdmin(userAccount, retrievedFinancial)) throw new NotAuthorizedToPerformActionException();
        retrievedFinancial.setTitle(financial.getTitle());
        retrievedFinancial.setDescription(financial.getDescription());
        retrievedFinancial.setAmount(financial.getAmount());
        retrievedFinancial.setCustomerId(financial.getCustomerId());

        return financialRepository.save(retrievedFinancial);
    }

    private boolean isNotCreatorOrAdmin(UserAccount userAccount, Financial retrievedFinancial) {
        return !retrievedFinancial.getCreatorId().equals(userAccount.getId()) || !userAccount.hasRole("ROLE_ADMIN");
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public List<Financial> getAllFinancial() {
        UserAccount userAccount = authenticationService.userAccountDetails();
        if (userAccount.hasAdminRole()) return financialRepository.findAll();
        return financialRepository.findAllByCreatorIdOrderByCreatedDateDesc(userAccount.getId());
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER')")
    public Financial getFinancialById(long id) throws NotAuthorizedToPerformActionException {
        Financial retrievedFinancial = financialRepository.getOne(id);
        if (isNotCreatorOrAdmin(authenticationService.userAccountDetails(), retrievedFinancial)) throw new NotAuthorizedToPerformActionException();
        return retrievedFinancial;
    }



}



