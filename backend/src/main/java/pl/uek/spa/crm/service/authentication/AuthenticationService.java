package pl.uek.spa.crm.service.authentication;

import pl.uek.spa.crm.model.UserAccount;

import java.util.Map;

public interface AuthenticationService {

    UserAccount createUserAccount(UserAccount newUser);
    UserAccount userAccountDetails();
    Map<String, Object> login(String username, String password);
}
