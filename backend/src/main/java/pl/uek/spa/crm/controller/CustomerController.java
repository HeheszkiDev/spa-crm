package pl.uek.spa.crm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.uek.spa.crm.model.Customer;
import pl.uek.spa.crm.service.authentication.NotAuthorizedToPerformActionException;
import pl.uek.spa.crm.service.customer.CustomerNotFoundException;
import pl.uek.spa.crm.service.customer.CustomerService;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(value = "*")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping(value = "/api/customer/")
    private ResponseEntity<List<Customer>> getAllCustomers(){
        List<Customer> customers = customerService.getAllCustomers();
        if (customers.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @GetMapping(value = "/api/customer/{id}")
    private ResponseEntity<Customer> getCustomer(@PathVariable("id") long id){
        Optional<Customer> customer = Optional.ofNullable(customerService.getCustomerById(id));
        if (customer.isPresent()) return new ResponseEntity<>(customer.get(), HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/api/customer/")
    private ResponseEntity<Customer> createCustomer(@RequestBody Customer customer){
        Optional<Customer> savedCustomer = Optional.ofNullable(customerService.saveCustomer(customer));
        if (savedCustomer.isPresent()) return new ResponseEntity<>(savedCustomer.get(), HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PutMapping(value = "/api/customer/{id}")
    private ResponseEntity<Customer> updateCustomer(@PathVariable("id") long id, @RequestBody Customer customer){
        try {
            Customer updatedCustomer = customerService.updateCustomer(id, customer);
            return new ResponseEntity<>(updatedCustomer, HttpStatus.OK);
        } catch (CustomerNotFoundException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        catch (NotAuthorizedToPerformActionException e){
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping(value = "/api/customer/{id}")
    private ResponseEntity<Void> deleteCustomer(@PathVariable("id") long id){
        try {
            customerService.deleteCustomer(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (CustomerNotFoundException e){
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        catch (NotAuthorizedToPerformActionException e){
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping(value = "/api/admin/customer/{id}")
    private ResponseEntity<List<Customer>> getAllCustomersByOwnerId(@PathVariable("id") long id){
        List<Customer> customers = customerService.findAllCustomersByOwnerId(id);
        if (customers.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }
}
