package pl.uek.spa.crm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.uek.spa.crm.controller.request.UpdatePasswordRequest;
import pl.uek.spa.crm.model.UserAccount;
import pl.uek.spa.crm.service.user.account.UserAccountService;

import java.util.List;

@RestController
public class UserAccountController {

    @Autowired
    private UserAccountService userAccountService;

    @GetMapping(value = "/api/users")
    public List<UserAccount> users() {
        return userAccountService.users();
    }

    @GetMapping(value = "/api/users/{id}")
    public ResponseEntity<UserAccount> userById(@PathVariable Long id) {
        UserAccount userAccount = userAccountService.userById(id);
        if (userAccount == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(userAccount, HttpStatus.OK);
        }
    }

    @DeleteMapping(value = "/api/users/{id}")
    public ResponseEntity<UserAccount> deleteUser(@PathVariable Long id) {
        UserAccount userAccount = userAccountService.deleteUser(id);
        if (userAccount == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        else {
            return new ResponseEntity<>(userAccount, HttpStatus.OK);
        }


    }

    @PostMapping(value = "/api/users")
    public ResponseEntity<UserAccount> createUser(@RequestBody UserAccount userAccount) {
        return new ResponseEntity<UserAccount>(userAccountService.createUser(userAccount), HttpStatus.CREATED);
    }

    @PutMapping(value = "/api/users")
    public ResponseEntity<UserAccount> updateUser(@RequestBody UserAccount userAccount) {
        return new ResponseEntity<>(userAccountService.updateUser(userAccount), HttpStatus.OK);
    }

    @PostMapping(value = "/api/users/settings")
    public ResponseEntity<UserAccount> updateUserData(@RequestBody UserAccount userAccount) {
        boolean success = userAccountService.updateUserData(userAccount);
        if (success){
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @PostMapping(value = "/api/users/settings/password")
    public ResponseEntity<UserAccount> changePassword(@RequestBody UpdatePasswordRequest request) {
        boolean success = userAccountService.changePassword(request);
        if (success){
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
