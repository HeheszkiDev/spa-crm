package pl.uek.spa.crm.repository.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.uek.spa.crm.model.Customer;

import java.util.List;
import java.util.Set;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>, CustomerRepositoryCustom{

    List<Customer> findAllByOwnerId(long ownerId);
    List<Customer> findAllByIdIn(Set<Long> id);
    Integer countByOwnerId(Long ownerId);
}
