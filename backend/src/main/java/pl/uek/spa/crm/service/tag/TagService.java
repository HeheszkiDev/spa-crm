package pl.uek.spa.crm.service.tag;

import pl.uek.spa.crm.model.Customer;
import pl.uek.spa.crm.model.Tag;

import java.util.List;

public interface TagService {

    List<Tag> getAllTags();
    void deleteTag(long tagId);
    void addCustomerToTag(Long customerId, String tagName);
    void removeCustomerFromTag(Long customerId, String tagName);
    List<Customer> findCustomersFromTag(String tagName);
    List<Tag> findCustomerTags(Long customerId);
}
