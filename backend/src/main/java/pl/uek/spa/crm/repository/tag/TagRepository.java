package pl.uek.spa.crm.repository.tag;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.uek.spa.crm.model.Tag;

import java.util.List;

@Repository
public interface TagRepository  extends JpaRepository<Tag, Long> {

    Tag findTagByTagName(String tagName);
    List<Tag> findAllByCustomersIdIsContaining(Long customerId);
    List<Tag> findAllByCreatorId(Long creatorId);
}
