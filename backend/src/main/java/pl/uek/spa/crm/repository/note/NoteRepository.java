package pl.uek.spa.crm.repository.note;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.uek.spa.crm.model.Note;

import java.util.List;

/**
 * Created by Tochi on 2017-04-30.
 */
@Repository
public interface NoteRepository extends JpaRepository<Note, Long>, NoteRepositoryCustom{

    List<Note> findAllByCreatorIdOrderByCreatedDateDesc(long id);
    Integer countByCreatorId(Long creatorId);

}
