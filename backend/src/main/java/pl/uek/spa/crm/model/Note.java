package pl.uek.spa.crm.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Tochi on 2017-04-30.
 */
@Entity
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title="none";
    @Column(length = 1024)
    private String description;

    private Long customerId;
    private Date createdDate;
    private Long creatorId;


    public Note() {
    }

    public Note(String title, String description, Long customerId, Date createdDate, Long creatorId) {
        this.title = title;
        this.description = description;
        this.customerId = customerId;
        this.createdDate = createdDate;
        this.creatorId = creatorId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }
}
