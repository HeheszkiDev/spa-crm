package pl.uek.spa.crm.repository.note;



import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.spa.crm.model.Address;
import pl.uek.spa.crm.model.Customer;
import pl.uek.spa.crm.model.Note;
import pl.uek.spa.crm.repository.customer.CustomerRepository;

import java.util.Date;
import java.util.List;

@DataJpaTest
@RunWith(SpringRunner.class)
public class NoteRepositoryTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private NoteRepository noteRepository;

    @Test
    @Transactional
    public void testSaveMethod() throws Exception {


        Address testAddress = Address.AddressBuilder.anAddress()
                .withCity("Kraków")
                .withStreet("Leśna 12")
                .withZipCode("32-321")
                .withCountry("Polska")
                .withRegion("Małopolska")
                .build();

        Customer customer = new Customer("Jan", "Kowalski", testAddress, "123-123-123", "test@mail.com", 1L);
        Date d= new Date();
        Note note=new Note("Tytuł","Opis notatki",customer.getId(),d,1L);

        customerRepository.save(customer);
        noteRepository.save(note);

        customerRepository.save(customer);

        //when
        List<Note> result = noteRepository.findAll();

        //then
        Assertions.assertThat(result).hasSize(1);
        Assertions.assertThat(result.get(0))
                .isNotNull()
                .hasFieldOrPropertyWithValue("title", "Tytuł")
                .hasFieldOrPropertyWithValue("description", "Opis notatki");

    }






}
