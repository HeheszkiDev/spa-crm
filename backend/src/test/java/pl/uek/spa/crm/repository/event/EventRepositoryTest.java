package pl.uek.spa.crm.repository.event;

import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.spa.crm.model.event.Event;
import pl.uek.spa.crm.model.event.EventType;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@DataJpaTest
@RunWith(SpringRunner.class)
public class EventRepositoryTest {


    @Autowired
    private EventTypeRepository typeRepository;

    @Autowired
    private EventRepository eventRepository;

    @Test
    @Transactional
    @Ignore public void testSaveAndRetrieveEvents() throws Exception {

        EventType phonecall = typeRepository.save(new EventType("phonecall"));

        eventRepository.save(new Event(1L, phonecall, "Unofficial call", "Unofficial call, about planned meeting details", new Date(), 1L));
        eventRepository.save(new Event(2L, phonecall, "Unofficial call 2", "Unofficial call, about planned meeting details", new Date(), 1L));

        Event testEvent = eventRepository.getOne(1L);
        Assertions.assertThat(testEvent)
                .hasFieldOrPropertyWithValue("eventType", phonecall)
                .hasFieldOrPropertyWithValue("name", "Unofficial call");

        Assertions.assertThat(testEvent.getEventType().getId()).isEqualTo(1L);

        System.out.println(testEvent);
    }

    @Test
    @Transactional
    @Ignore
    public void testFindAllEventsByCustomerId() throws Exception {

        EventType phonecall = typeRepository.save(new EventType("phonecall"));
        EventType meeting = typeRepository.save(new EventType("meeting"));

        LocalDateTime dateTimeOne = LocalDateTime.of(2017, 4,30,15,0);
        LocalDateTime dateTimeTwo = LocalDateTime.of(2017, 5,1,10,0);
        LocalDateTime dateTimeThree = LocalDateTime.of(2017, 5,2,15,30);
        eventRepository.save(new Event(1L, phonecall, "Unofficial", "Unofficial call, about planned meeting details", Date.from(dateTimeOne.atZone(ZoneId.systemDefault()).toInstant()), 1L));
        eventRepository.save(new Event(1L, meeting, "First meet", "First meeting with important client", Date.from(dateTimeTwo.atZone(ZoneId.systemDefault()).toInstant()), 1L));
        eventRepository.save(new Event(1L, meeting, "Second meet", "Second meeting with important client", Date.from(dateTimeThree.atZone(ZoneId.systemDefault()).toInstant()), 1L));
        eventRepository.save(new Event(2L, phonecall, "Unofficial call 2", "Unofficial call, about planned meeting details", new Date(), 1L));

        List<Event> result = eventRepository.findAllByCustomerIdOrderByEventDateDesc(1L);
        Assertions.assertThat(result)
                .hasSize(3)
                .filteredOn("customerId", 1L);

        result.forEach(System.out::println);
    }
}