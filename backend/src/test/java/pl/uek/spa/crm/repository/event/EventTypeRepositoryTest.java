package pl.uek.spa.crm.repository.event;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.spa.crm.model.event.EventType;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@DataJpaTest
@RunWith(SpringRunner.class)
public class EventTypeRepositoryTest {

    @Autowired
    private EventTypeRepository typeRepository;

    @Test
    @Transactional
    public void testSaveAndRetrieve() throws Exception {

        typeRepository.save(new EventType("phonecall"));
        typeRepository.save(new EventType("meeting"));
        typeRepository.save(new EventType("webinar"));
        typeRepository.save(new EventType("email"));
        typeRepository.save(new EventType("offer sent"));

        List<EventType> result = typeRepository.findAll();
        Assertions.assertThat(result)
                .hasSize(5);

        result.forEach(x-> Assertions.assertThat(x.getEventName()).isIn(Arrays.asList("phonecall",
                "meeting", "webinar", "email", "offer sent")));
    }
}