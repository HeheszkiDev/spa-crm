package pl.uek.spa.crm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.uek.spa.crm.repository.customer.CustomerRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpaCrmApplicationTests {

	@Autowired
	private CustomerRepository customerRepository;

	@Test
	public void contextLoads() {

	}

}
