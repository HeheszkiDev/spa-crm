package pl.uek.spa.crm.repository.customer;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.spa.crm.model.Address;
import pl.uek.spa.crm.model.Customer;

import java.util.List;

@DataJpaTest
@RunWith(SpringRunner.class)
public class CustomerRepositoryTest {


    @Autowired
    private CustomerRepository customerRepository;

    @Test
    @Transactional
    public void testSaveMethod() throws Exception {

        //given
        Address testAddress = Address.AddressBuilder.anAddress()
                .withCity("Kraków")
                .withStreet("Leśna 12")
                .withZipCode("32-321")
                .withCountry("Polska")
                .withRegion("Małopolska")
                .build();

        Customer customer = new Customer("Jan", "Kowalski", testAddress, "123-123-123", "test@mail.com", 1L);

        customerRepository.save(customer);

        //when
        List<Customer> result = customerRepository.findAll();

        //then
        Assertions.assertThat(result).hasSize(1);
        Assertions.assertThat(result.get(0))
                .isNotNull()
                .hasFieldOrPropertyWithValue("firstName", "Jan")
                .hasFieldOrPropertyWithValue("lastName", "Kowalski")
                .hasFieldOrPropertyWithValue("address", testAddress);

    }
}